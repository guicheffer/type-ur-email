const { config } = require('./package.json')

// Please, have your kickbox apikey in a node env to get this working
const kickbox = require('kickbox').client(process.env.KICKBOX_APIKEY).kickbox()

const express = require('express')
const path = require('path')
const PORT = process.env.PORT || config.port || 8080

express()
  .use(express.static(path.join(__dirname, config.path.dist)))
  .get('/kickbox', (req, res) => {
    const email = req.query.email || ''

    if (!email) return res.json({ err: 'no email' })

    return kickbox.verify(email, (err, response) => res.json(response.body))
  })
  .listen(PORT, () => console.log(`Listening on ${ PORT }`))
