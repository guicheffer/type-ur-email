# type your email
> Code test in react+webpack+html+js|babel|es6+stylus+jest+kickbox!+bla+bla+bla for invoice simple

## Introduction
This mini-project is to shows up my knowledge with hype frameworks, libraries, React data treatment, styles, javascript, modern bundles and etc.

![image](https://user-images.githubusercontent.com/5280832/35598504-f25b835c-060a-11e8-9467-270f3b549442.png)

**Preview**:
_soon_

## Install
- To install things and dependencies: `npm i`, simply do it!

## Run
- To run the project, just `npm start`;
- To watch for updates on the project and build the project, just `npm run dev`;
- To have the project built and ready for prod env, simply `npm run deploy`;
- To validate an email, you gotta have an apikey settled up on your environment 🙂

## Pack
- Please, to make it deliverable, run `npm pack`
- If you want to review the test, just `make install` then `make serve`! :)

## Test
- Please, `make test` for e2e tests and `make test-unit` to watch unit tests;
- `make tests` should run both. They run exactly before a deploy on any server 🙂

**You feeling tired to do those run tasks?**
Just access [here](https://type-ur-email.herokuapp.com/): https://type-ur-email.herokuapp.com/
⬆️ &nbsp; This project was deployed on [Heroku](https://dashboard.heroku.com/) 🙂

## Considerations:

### Some of them can be seen here: https://gitlab.com/guicheffer/type-ur-email/blob/master/src/app/home/email-field.js#L14
**BUT**, if you don't got access to this, please read those:

- Technical thingy: Sorry for not using typescript, I write es6 better than using typescript for example, please consider it;
- I hope you(all), the reviewers, enjoy the way we save on localStorage after a validation inputed on email field component which is valid. This way we can guarantee users are always seeing the correct information;
- This component will load its necessary files to be running their logic information and tools such as popular emails to help itself finding the best way to present mistakes and successful messages!


### What makes/made me happy on every project (including this code challenge)
- CSS Animations;
- Order attributes in CSS alphabetically;
- Atomic commits;
- Commit on master ... (kidding, in general...**PRs**, **pls** 😜);
- Usage of [caniuse.com](caniuse.com);
- Usage of new technologies, choice among frameworks and libraries;
	- Vue.js, React, Angular and Knockout...love 'em 🖤
- Usage of Mobile First designing and development;
- ...To Study in general;
- Usage of emojis ❤️ &nbsp; (I really **LOVE** this thingy);

**Extras**:

- [ ] Style
	- [ ] Add a critical style into pages
	- [ ] Create views with A/B tests config
- [ ] Setup
	- [ ] Circleci, travis or similar tool
- [ ] Improve error pages
	- [ ] 404 style
	- [ ] 500 style
- [ ] Add needed unit tests
- [ ] Add extra e2e tests
- [x] Make a nice-to-read README.md ❤️
