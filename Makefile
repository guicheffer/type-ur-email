PKG:=npm
DEPLOY:=build:prod
GOURCE:=gource

help:
	@echo
	@echo "✍🏽  Please use 'make <target>' where <target> is one of the commands below:"
	@echo
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e "s/\\$$//" | sed -e "s/##//"
	@echo

# ------------------------------------------------------------------------------------ #

build: ## build locally the files
	$(PKG) run build

build-prod: ## build on a prod-version
	$(PKG) run $(DEPLOY)

change-version: ## change the project version
	$(PKG) version

clean: ## make it clean, pls sir
	$(PKG) run clean-files

deploy:
	$(PKG) run deploy

install: ## install missing dependencies
	$(PKG) i

pack: ## pack project in case of develirable
	$(PKG) pack

server: ## runs locally on a 3000 port pre-defined on package.json
	$(PKG) run server

run: server
serve: run

start: clean build run

test: ## tests type-ur-email.com e2e tests
	$(PKG) test

test-unit: ## runs type-ur-email.com unit tests
	$(PKG) run test:unit

test-unit-watch: ## watches type-ur-email.com unit tests modification
	$(PKG) run test:unit-watch

tests: test test-unit

watch: ## watch what's important to
	$(PKG) run watch

gource:
	@echo "No '$(GOURCE)' task was settled up 😞"
