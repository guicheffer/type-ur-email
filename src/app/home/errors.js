/*-
 * ❌ Errors component
 *
-*/

import React, { Component } from 'react'

import mapErrors from './errors.map'

class Errors extends Component {
  constructor (props) {
    super(props)

    this.props = props
  }

  getErrors () {
    // We reverse it, because new errors are more relevant
    //  than others that already have been done
    return mapErrors(this.props.errors, this.props.email).reverse()
  }

  render () {
    return (
      <ul>
        { this.getErrors().map((error, index) => (
          <li
            className={ this.props.className }
            dangerouslySetInnerHTML={ { __html: error } }
            key={ index }
          ></li>
        )) }
      </ul>
    )
  }
}

export default Errors
