/*-
 * 🙂 Get email properties
 *
 * This is how we get username and domain from an email
 *
-*/

const getEmailProperties = email => ({
  username: (email.match(/.*@/) || []).length ? email.match(/.*@/)[0].replace(/@/, '') : null,
  domain: email.replace(/.*@/, '') || null,
})

export default getEmailProperties
