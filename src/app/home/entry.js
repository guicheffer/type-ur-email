/*-
 * ⭐️ HomeEntry
 *
 * This is the home entry file for star wars code challenge.
 *
-*/

import React from 'react'
import ReactDOM from 'react-dom'

import AbstractEntry from '../core/entry'

import EmailField from './email-field'

// eslint-disable-next-line no-undef
const browser = window

class HomeEntry extends AbstractEntry {
  render () {
    ReactDOM.render(
      <EmailField
        defaultEmptyEmailValue={ this.initilizationData.defaultEmptyEmailValue }
        defaultInputEmailValue={ this.initilizationData.defaultInputEmailValue }
      />,
      this.ui.emailField,
    )
  }

  start ({ initilizationData }) {
    this.initilizationData = initilizationData

    this.ui = {
      emailField: browser.document.querySelector('#app-type-ur-email-field'),
    }

    this.render()
  }
}

export default HomeEntry
