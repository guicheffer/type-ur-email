/*-
 * ⭐️ Email field (needs refactoring)
 *
 * This is our email field component
 * This is where we can truly add many checks and validation needings
 *  on each pice of the code. `errors.map` will help users to receive
 *  a good feedback message in case of mistake or even of a valid one.
 *
 * There are some stateless component which has not been added into
 *  this child component from homepage, this `EmailField` will look
 *  after on its responsabilities such as validation and email suggestions,
 *  where users can be sure of typos and domain suggestions into their emails
 *
 * Considerations:
 * - I hope you(all), the reviewers, enjoy the way we save on localStorage
 *  after a validation inputed on email field component which is valid.
 *  This way we can guarantee users are always seeing the correct information.
 * - This component will load its necessary files to be running their logic information
 *  and tools such as popular emails to help itself finding the best way
 *  to present mistakes and successful messages!
 * - I like emojis, already told that on `README.md` file, didn't I?
 * - I hope you(all) enjoy!
 *
-*/

import _ from 'lodash'
import axios from 'axios'
import React, { Component } from 'react'

import localStorage from '../helpers/local-storage'

import Errors from './errors'
import getEmailProperties from './utils'
import popularEmailsList from './popular-emails'

// eslint-disable-next-line no-undef
const browser = window

const DOMAIN_REGEX = /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$/
const DEFAULT_MAX_SUGGESTIONS = 10
const KICKBOX_ENDPOINT_URL = '/kickbox'
const DEFAULT_MSG_COULDNT_CHECK = '❌  we don\'t recognize ur email, we couldn\'t check it; please try again:'
const DEFAULT_MSG_DIDUMEAN_PREFIX = '⚠️  did u mean '

class EmailField extends Component {
  constructor (props) {
    super(props)

    this.state = {
      didUMean: null,
      email: '',
      errors: [],
      isValidating: false,
      unknowEmail: false,
      validated: false,
    }
  }

  // While this already has been mount, we can be sure
  //  in focusing the email input into the homepage :)
  componentDidMount () {
    // When typing something outside the input,
    //  focus on the main input email field
    browser.document.body.addEventListener('keydown', () => {
      if (this.input !== browser.document.activeElement) {
        this.input.focus()
      }
    })

    // Don't allow pasting on email input field
    this.input.addEventListener('paste', event => event.preventDefault())
  }

  // This function resets needed states and
  //  any differnt feedback screen on homepage
  reset () {
    this.setState({
      didUMean: null,
      suggestions: [],
      validated: false,
    })

    browser.document.body.classList.remove('success-bg')
  }

  // All changes will be prepared and better-read here.
  // This means we can validate only when needed and set an email state
  //  into this component
  handleChange (change) {
    const email = (typeof change === 'string') ? change : change.target.value
    if (email !== this.state.email) this.reset()

    this.activateFullValidation(email)
    this.setState({ email })
  }

  // This is the one we trust we can feedback a success message 🙂
  success () {
    browser.document.body.classList.add('success-bg')
    this.setState({ validated: true })
  }

  // This function returns true in case of a success validation
  validate (condition, error) {
    if (condition) {
      this.state.suggestions = []
      if (this.state.errors.indexOf(error) === -1) this.state.errors.push(error)
      return false
    }

    _.remove(this.state.errors, item => (item === error))
    return true
  }

  // This is the validation function which will
  //  check probabilities of mistakes onto typed email
  //
  // In case of an error, this will dispatch into validate common function
  //  a error slug for more details of the mistake commited on
  //  type timing. This also checks a valid email on localStorage helper,
  //  that means if user already validate its email, this will dispatch a
  //  green screen with a success feedback :)
  activateFullValidation (email) {
    if (!this.validate(!email, 'missing')) { return false }
    if (!this.validate((email.indexOf('@') === -1), 'at')) { return false }
    if (!this.validate((email.split('@').length > 2), 'multiple')) { return false }

    const { username, domain } = getEmailProperties(email)
    if (!this.validate(!username, 'username')) { return false }

    if (!this.validate((!DOMAIN_REGEX.test(domain)), 'domain')) {
      this.setState({ suggestions: popularEmailsList })
      return false
    }

    const validEmails = localStorage.getItem('validEmails', true) || []
    if (validEmails.indexOf(email.toLowerCase()) !== -1) this.success()

    this.setState({ suggestions: [] })
    return null
  }

  // This fn will be executed after user's suggestions selection
  // In case of a chosen result, this will render input field text with an updated one :)
  selectedSuggestion (event) {
    const { username } = getEmailProperties(this.state.email)
    const chosenDomain = event.target.getAttribute('data-value').replace(/@/, '')
    const updatedEmail = `${username}@${chosenDomain}`

    this.input.value = updatedEmail
    this.handleChange(updatedEmail)
  }

  // Just not for hiting the kickbox API again, we save it into a
  //  localStorage since we can access it later based on user's validation action
  saveEmail (email) {
    const validEmails = localStorage.getItem('validEmails', true) || []
    validEmails.push(email.toLowerCase())

    return localStorage.setItem('validEmails', validEmails)
  }

  // This one will get into `kickbox` website and check if user's email
  //  is really valid, if so, screen will be appearing as green as a jungle
  //  to show uxly a success result :)
  finalValidate (event) {
    if (this.state.validated) return event.preventDefault()
    this.setState({ isValidating: true, unknowEmail: false })

    axios.get(`${KICKBOX_ENDPOINT_URL}?email=${this.state.email}`)
      .then((res) => {
        if (res.data.did_you_mean) return this.setState({ didUMean: res.data.did_you_mean })

        if (res.data.result !== 'deliverable') return this.setState({ unknowEmail: true })

        this.success()
        return this.saveEmail(this.state.email)
      })
      // eslint-disable-next-line no-console
      .catch(error => console.error(error.response.status))
      .then(() => this.setState({ isValidating: false }))

    return event.preventDefault()
  }

  // It gets available suggestions based on user's domain predictable typing
  getAvailableSuggestions () {
    const { domain } = getEmailProperties(this.state.email)
    const suggestions =
      domain ?
        _.filter(this.state.suggestions, emailSuggestions =>
          (emailSuggestions.indexOf(domain.toLowerCase()) !== -1))
        : this.state.suggestions

    if (!suggestions) return []
    return suggestions.slice(0, DEFAULT_MAX_SUGGESTIONS)
  }

  // This gets the current label value with its defaults and confguration
  getLabelValue () {
    if (this.state.validated) return this.props.defaultInputEmailValue
    if (this.state.email === '') return this.props.defaultEmptyEmailValue
    if (this.state.didUMean) return DEFAULT_MSG_DIDUMEAN_PREFIX
    if (this.state.unknowEmail) return DEFAULT_MSG_COULDNT_CHECK
    return this.props.defaultInputEmailValue
  }

  // This function will provide us an email
  //  suggestion in case of kickbox sends us another email guess
  handleEmailSuggestion (event) {
    const emailSuggestion = event.target.getAttribute('data-suggestion')

    if (emailSuggestion) {
      this.input.value = emailSuggestion
      this.handleChange(emailSuggestion)
    }
  }

  // Oficial component rendering (I know could break into many other pices, but I didn't /: )
  render () {
    return (
      <form
        className="email-field__content"
        data-is-validating={ this.state.isValidating }
        data-validation-text="Validating..."
      >
        <div className="email-field__container" disabled={ this.state.isValidating }>
          <input
            autoCapitalize="off"
            autoComplete="off"
            autoCorrect="off"
            spellCheck="false"
            className="email-field__input"
            id="email"
            maxLength="320"
            onKeyUp={ this.handleChange.bind(this) }
            placeholder={ this.props.defaultEmptyEmailValue }
            ref={(input) => { this.input = input }}
            type="email"
          />
          <label
            className="email-field__label" data-fixed={ `${this.state.email !== '' ? true : ''}` }
            data-value={ this.getLabelValue() }
            data-suggestion={ this.state.didUMean }
            onClick={ this.handleEmailSuggestion.bind(this) }
            htmlFor="email"
          ></label>

          <div className="email-field__suggestions" data-active={ `${!_.isEmpty(this.state.suggestions) ? true : ''}` }>
            { !_.isEmpty(this.getAvailableSuggestions()) ?
              <ul>
                { this.getAvailableSuggestions().map((suggestion, index) => (
                  <li
                    key={ index }
                    className="email-field__suggestion"
                    data-value={suggestion}
                    onClick={ this.selectedSuggestion.bind(this) }>
                  { suggestion }
                  </li>
                ))}
              </ul> : ''
            }
          </div>

          <div className="email-field__errors">
            { !_.isEmpty(this.state.errors) ?
              <Errors
                className="email-field__error"
                email={ this.state.email }
                errors={ this.state.errors }
              /> : ''
            }
          </div>

          <button
            className="email-field__submit"
            disabled={ this.state.email === '' || !_.isEmpty(this.state.errors) || this.state.isValidating }
            onClick={ this.finalValidate.bind(this) }
            data-validated={ this.state.validated }
            type="submit">
          { this.state.validated ? 'ur email is valid 🙂' : 'Validate'}
          </button>
        </div>
      </form>
    )
  }
}

export default EmailField
