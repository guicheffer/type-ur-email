/*-
 * ⚠️ Errors mapper
 *
 * This is our error mapper
 *
-*/

import _ from 'lodash'

import getEmailProperties from './utils'

const DEFAULT_PREFIX_ICON = '👀'
const DEFAULT_USERNAME = 'tiny'

export default (errors, email) => {
  const errorsMapper = {
    missing: {
      prefix: '⚠️',
      title: 'Please, an email has to be filled above!',
    },
    at: {
      title: 'There is no email if there is no `at (@)` on the email!',
    },
    multiple: {
      prefix: '🤔',
      title: 'This time, there are many `ats (@\'s)` on the email, aren\' there?',
    },
    username: {
      title: 'Missing username or recipient on your email 🙂',
    },
    domain: {
      title:
        `Don't act too smart with me, ${_.get(getEmailProperties(email), 'username') || DEFAULT_USERNAME}!\
        <small>Type your email's domain correctly or choose one above</small>!`,
    },
  }

  return _.map(errors, (error) => {
    if (!errorsMapper[error]) return ''

    const prefix = `<span>${errorsMapper[error].prefix || DEFAULT_PREFIX_ICON}</span>`
    return `${prefix} ${errorsMapper[error].title}`
  })
}
